/* (C)2022 */
package domain.service;

import domain.model.Article;

import java.util.List;

public interface ArticleService {

	void create(Article article);

	void update(Article article);

	void delete(Long articleId);

	Article get(Long articleId);

	List<Article> getByList(List<Long> listArticleIds);

	List<Article> getAll();

	Long exist(Article article);

	Long count();

}
