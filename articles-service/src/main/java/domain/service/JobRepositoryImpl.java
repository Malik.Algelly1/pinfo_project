/* (C)2022 */
package domain.service;

import domain.model.Job;
import java.util.List;

import javax.inject.Inject;

import javax.transaction.Transactional;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.EntityNotFoundException;
import javax.persistence.EntityExistsException;

import javax.enterprise.context.ApplicationScoped;

import java.util.Objects;

@ApplicationScoped
public class JobRepositoryImpl implements JobRepository {

	@Inject
	EntityManager em;

	@Inject
	ArticleService articleService;

	public JobRepositoryImpl() {
	}

	public JobRepositoryImpl(EntityManager em) {
		this();
		this.em = em;
	}

	@Override
	@Transactional
	public void create(Job job) {
		Job jobGet = em.find(Job.class, job.getJobId());
		if (Objects.isNull(jobGet)) {
			em.persist(job);
		} else {
			throw new EntityExistsException("Already exists: Did not add the job id: " + job.getJobId());
		}
	}

	@Override
	@Transactional
	public void update(Job job) {
		Job jobGet = em.find(Job.class, job.getJobId());
		if (Objects.nonNull(jobGet)) {
			em.merge(job);
		} else {
			throw new EntityNotFoundException(
					"Not in the database: Failed to update the job id: " + job.getJobId());
		}
	}

	@Override
	public Job get(Long jobId) {
		Job jobGet = em.find(Job.class, jobId);
		if (Objects.nonNull(jobGet)) {
			return jobGet;
		} else {
			throw new EntityNotFoundException(
					"Not in the database: Failed to get the job id: " + jobId);
		}
	}

	@Override
	public Boolean exist(Long jobId) {
		return Objects.nonNull(em.find(Job.class, jobId));
	}

	@Override
	@Transactional
	public void delete(Long jobId) {

		Job jobGet = em.find(Job.class, jobId);

		if (Objects.nonNull(jobGet)) {

			jobGet.getArticles().forEach(article -> {
				article.getJobs().remove(jobGet);
				articleService.update(article);
			});
			em.remove(jobGet);
		} else {
			throw new EntityNotFoundException(
					"Not in the database: Failed to delete the job id: " + jobId);
		}
	}

	@Override
	public Long count() {
		CriteriaBuilder qb = em.getCriteriaBuilder();
		CriteriaQuery<Long> cq = qb.createQuery(Long.class);

		cq.select(qb.count(cq.from(Job.class)));
		return em.createQuery(cq).getSingleResult();
	}

	@Override
	public List<Job> getAll() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Job> cq = cb.createQuery(Job.class);
		cq.from(Job.class);
		return em.createQuery(cq).getResultList();
	}
}
