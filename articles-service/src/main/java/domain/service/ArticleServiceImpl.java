/* (C)2022 */
package domain.service;

import domain.model.Article;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import javax.transaction.Transactional;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.NoResultException;
import javax.persistence.EntityNotFoundException;
import javax.persistence.EntityExistsException;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class ArticleServiceImpl implements ArticleService {

	@Inject
	EntityManager em;

	@Inject
	JobRepository jobRepository;

	private static final String ARTICLEID = "articleId";

	public ArticleServiceImpl() {
	}

	public ArticleServiceImpl(EntityManager em) {
		this();
		this.em = em;
	}

	@Override
	@Transactional
	public void create(Article article) {
		Long articleId = this.exist(article);
		if (Objects.isNull(articleId)) {
			em.persist(article);
		} else {
			throw new EntityExistsException(
					"Already exists: Did not add the article id: " + article.getArticleId());
		}
	}

	@Override
	@Transactional
	public void update(Article article) {
		Article articleGet = em.find(Article.class, article.getArticleId());
		if (Objects.nonNull(articleGet)) {
			em.merge(article);
		} else {
			throw new EntityNotFoundException(
					"Not in database: Failed to update the article id: " + article.getArticleId());
		}
	}

	@Override
	@Transactional
	public Long exist(Article article) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Long> cq = cb.createQuery(Long.class);
		Root<Article> root = cq.from(Article.class);
		cq.select(root.get(ARTICLEID));

		if (article.getDoi() != "")
			cq.where(cb.equal(root.get("doi"), article.getDoi()));
		else
			cq.where(cb.equal(root.get("articleAbstract"), article.getArticleAbstract()));

		Long articleId = null;
		try {
			articleId = em.createQuery(cq).getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}

		return articleId;
	}

	@Override
	public Article get(Long articleId) {
		Article article = em.find(Article.class, articleId);
		if (Objects.nonNull(article)) {
			return article;
		} else {
			throw new EntityNotFoundException(
					"Not in database: Failed to get the article id: " + articleId);
		}
	}

	@Override
	public List<Article> getByList(List<Long> listArticleIds) {

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Article> cq = cb.createQuery(Article.class);

		Root<Article> root = cq.from(Article.class);
		cq.select(root);
		cq.where(root.get(ARTICLEID).in(listArticleIds));

		return em.createQuery(cq).getResultList();
	}

	@Override
	public List<Article> getAll() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Article> cq = cb.createQuery(Article.class);
		cq.from(Article.class);
		return em.createQuery(cq).getResultList();
	}

	@Override
	@Transactional
	public void delete(Long articleId) {

		Article articleGet = em.find(Article.class, articleId);

		if (Objects.nonNull(articleGet)) {

			articleGet.getJobs().forEach(job -> {
				job.getArticles().remove(articleGet);
				jobRepository.update(job);
			});
			em.remove(articleGet);
		} else {
			throw new EntityNotFoundException(
					"Not in database: Failed to delete the article id: " + articleId);
		}
	}

	@Override
	public Long count() {
		CriteriaBuilder qb = em.getCriteriaBuilder();
		CriteriaQuery<Long> cq = qb.createQuery(Long.class);

		cq.select(qb.count(cq.from(Article.class)));
		return em.createQuery(cq).getSingleResult();
	}

}
