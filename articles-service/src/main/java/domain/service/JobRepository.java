/* (C)2022 */
package domain.service;

import domain.model.Job;

import java.util.List;

public interface JobRepository {

	void create(Job job);

	void update(Job job);

	void delete(Long jobId);

	Job get(Long jobId);

	List<Job> getAll();

	Boolean exist(Long jobId);

	Long count();

}
