/* (C)2022 */
package domain.model;

import java.util.HashSet;
import java.util.Set;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.FetchType;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Entity
@NoArgsConstructor
public class Job {

	public Job(Long jobId) {
		this.jobId = jobId;
	}

	@Id
	private Long jobId;

	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
	@JoinTable(name = "jobArticle", joinColumns = @JoinColumn(name = "jobId"), inverseJoinColumns = @JoinColumn(name = "articleId"))
	@EqualsAndHashCode.Exclude
	@JsonIgnore
	@ToString.Exclude
	private Set<Article> articles = new HashSet<>();

	public List<Long> articlesToArticleIds() {
		return articles.stream().map(Article::getArticleId).toList();
	}

	public Boolean hasArticle(Long articleId) {
		return articles.stream().map(Article::getArticleId).anyMatch(articleId::equals);
	}
}
