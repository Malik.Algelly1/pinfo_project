/* (C)2022 */
package domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ArticleReception {
	private String doi;
	private String title;
	private String articleAbstract;
	private String url;
	private Long jobId;
}
