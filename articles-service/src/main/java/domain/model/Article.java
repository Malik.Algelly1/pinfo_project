/* (C)2022 */
package domain.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import javax.persistence.ManyToMany;

import javax.persistence.FetchType;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
public class Article {

	public Article(String doi, String title, String articleAbstract, String url) {
		this.doi = doi;
		this.title = title;
		this.articleAbstract = articleAbstract;
		this.url = url;
	}

	@Id
	@SequenceGenerator(name = "ARTICLE_SEQ", sequenceName = "ARTICLE_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ARTICLE_SEQ")
	private Long articleId;

	private String doi;

	@NotNull
	private String title;

	@NotNull
	private String articleAbstract;

	@NotNull
	@EqualsAndHashCode.Exclude
	private String url;

	@ManyToMany(mappedBy = "articles", fetch = FetchType.EAGER)
	@EqualsAndHashCode.Exclude
	Set<Job> jobs = new HashSet<>();

	public Boolean hasJob(Long jobId) {
		return jobs.stream().map(Job::getJobId).anyMatch(jobId::equals);
	}

}
