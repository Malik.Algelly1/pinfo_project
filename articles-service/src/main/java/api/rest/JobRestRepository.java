/* (C)2022 */
package api.rest;

import domain.model.Job;
import domain.model.Article;
import domain.service.JobRepository;
import domain.service.ArticleService;

import java.util.List;

import javax.persistence.EntityNotFoundException;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.annotation.security.PermitAll;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import org.eclipse.microprofile.jwt.JsonWebToken;

@ApplicationScoped
@Path("/jobs")
public class JobRestRepository {

	@Inject
	JobRepository jobRepository;

	@Inject
	ArticleService articleService;

	@Inject
	JsonWebToken jwt;

	@GET
	@PermitAll
	@Path("{jobId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@PathParam("jobId") Long jobId) {

		List<Article> articles;
		Job job = new Job();

		try {
			job = jobRepository.get(jobId);
			articles = articleService.getByList(job.articlesToArticleIds());
		} catch (EntityNotFoundException e) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}

		return Response.ok(articles).build();
	}

	@GET
	@PermitAll
	@Path("count")
	@Produces(MediaType.APPLICATION_JSON)
	public Response count() {
		return Response.ok(jobRepository.count()).build();
	}

}
