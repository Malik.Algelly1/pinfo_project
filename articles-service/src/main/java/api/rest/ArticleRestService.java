/* (C)2022 */
package api.rest;

import domain.model.Article;
import domain.service.ArticleService;

import java.util.List;
import java.util.Objects;

import javax.persistence.EntityNotFoundException;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.annotation.security.PermitAll;

import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.jwt.JsonWebToken;

@ApplicationScoped
@Path("/articles")
public class ArticleRestService {

	@Inject
	ArticleService articleService;

	@Inject
	JsonWebToken jwt;

	@GET
	@PermitAll
	@Path("{articleId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response get(@PathParam("articleId") Long articleId) {
		Article article = new Article();
		try {
			article = articleService.get(articleId);
		} catch (EntityNotFoundException e) {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
		return Response.ok(article).build();
	}

	@GET
	@PermitAll
	@Path("count")
	@Produces(MediaType.APPLICATION_JSON)
	public Response count() {
		return Response.ok(articleService.count()).build();
	}

	@GET
	@PermitAll
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getByList(List<Long> articlesIds) {
		List<Article> articles = articleService.getByList(articlesIds);
		if (Objects.nonNull(articles)) {
			return Response.ok(articles).build();
		} else {
			return Response.status(Response.Status.NOT_FOUND).build();
		}
	}
}
