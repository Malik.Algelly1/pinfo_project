/* (C)2022 */
package api.msg;

import domain.model.Article;
import domain.model.ArticleReception;
import domain.model.Job;

import java.util.Objects;

import domain.service.ArticleService;
import domain.service.JobRepository;
import io.smallrye.reactive.messaging.annotations.Blocking;

import javax.inject.Inject;
import javax.enterprise.context.ApplicationScoped;

import org.eclipse.microprofile.reactive.messaging.Incoming;

@ApplicationScoped
public class ArticleConsumer {

	@Inject
	ArticleService articleService;

	@Inject
	JobRepository jobRepository;

	@Incoming("articles")
	@Blocking
	public void addData(ArticleReception articlesR) {

		Long jobId = articlesR.getJobId();
		Job job = new Job(jobId);

		if (Boolean.FALSE.equals(jobRepository.exist(jobId)))
			jobRepository.create(job);
		else
			job = jobRepository.get(jobId);

		Article article = new Article(articlesR.getDoi(), articlesR.getTitle(), articlesR.getArticleAbstract(),
				articlesR.getUrl());

		article.getJobs().add(job);

		Long articleId = articleService.exist(article);

		if (Objects.nonNull(articleId)) {
			article.setArticleId(articleId);
			articleService.update(article);
		} else {
			articleService.create(article);
		}
		job.getArticles().add(article);
		jobRepository.update(job);
	}

}
