drop table Article if exists;
drop sequence if exists ARTICLE_SEQ;
create sequence ARTICLE_SEQ start with 1 increment by 1;
drop table Job if exists;
drop table JobArticle if exists;

create table Article (
    articleId Bigint(255) not null,
    doi varchar(255),
    title varchar(255),
    articleAbstract varchar(255),
    url varchar(255),
    primary key (articleId)
);

create table Job (
    jobId Bigint(255) not null,
    primary key (jobId)
);

create table JobArticle (
    articleId Bigint(255) not null,
    jobId Bigint(255) not null,
    primary key (articleId, jobId)
);

INSERT INTO ARTICLE (articleId, doi, title, articleAbstract, url) VALUES (ARTICLE_SEQ.nextval,'doi1','Title1','Abstract1','Url1');
INSERT INTO ARTICLE (articleId, doi, title, articleAbstract, url) VALUES (ARTICLE_SEQ.nextval,'doi2','Title2','Abstract2','Url2');
INSERT INTO ARTICLE (articleId, doi, title, articleAbstract, url) VALUES (ARTICLE_SEQ.nextval,'doi3','Title3','Abstract3','Url3');
INSERT INTO ARTICLE (articleId, doi, title, articleAbstract, url) VALUES (ARTICLE_SEQ.nextval,'doi4','Title4','Abstract4','Url4');

INSERT INTO JOB (jobId) VALUES (1);

INSERT INTO JOBARTICLE (articleId, jobId) VALUES (3, 1);
INSERT INTO JOBARTICLE (articleId, jobId) VALUES (4, 1);
