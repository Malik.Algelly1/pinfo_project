/* (C)2022 */
package domain.service;

import domain.model.Article;
import domain.model.Job;

import java.util.List;
import java.util.ArrayList;

import javax.inject.Inject;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.EntityExistsException;

import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.RandomStringUtils;

import org.junit.jupiter.api.Test;
import io.quarkus.test.junit.QuarkusTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

@QuarkusTest
class ArticleServiceImplTest {

	@Inject
	EntityManager em;

	@Inject
	ArticleService articleService;

	@Inject
	JobRepository jobRepository;

	@Test
	void testGet() {
		Article article = articleService.get(1L);
		assertEquals("Title1", article.getTitle());
	}

	@Test
	void testGetFail() {
		Long articleIdNotExist = articleService.count() + 1L;
		assertThrows(EntityNotFoundException.class, () -> {
			articleService.get(articleIdNotExist);
		});
	}

	@Test
	void testGetByList() {

		List<Long> articleIds = new ArrayList<>();
		articleIds.add(1L);
		articleIds.add(2L);
		articleIds.add(3L);

		List<Article> articles = articleService.getByList(articleIds);

		assertEquals(3, articles.size());
	}

	@Test
	void testCreate() {

		Long count = articleService.count();

		Article article = getRandomArticle();

		articleService.create(article);

		Long articleId = article.getArticleId();
		Article articleGet = articleService.get(articleId);

		assertEquals(count + 1L, articleService.count());
		assertEquals(article.getTitle(), articleGet.getTitle());
	}

	@Test
	void testCreateFail() {
		Article article = getRandomArticle();
		articleService.create(article);
		assertThrows(EntityExistsException.class, () -> {
			articleService.create(article);
		});
	}

	@Test
	void testCreateWithJob() {

		Long count = articleService.count();

		Long jobId = count + 1L;

		Article article = getRandomArticle();
		Job job = new Job(jobId);

		articleService.create(article);
		jobRepository.create(job);

		job.getArticles().add(article);
		article.getJobs().add(job);

		jobRepository.update(job);

		Long articleId = article.getArticleId();
		Article articleGet = articleService.get(articleId);
		Job jobGet = jobRepository.get(jobId);

		assertEquals(count + 1L, articleService.count());
		assertEquals(article.getTitle(), articleGet.getTitle());

		Boolean jobTrue = jobGet.hasArticle(articleId);
		Boolean articleTrue = articleGet.hasJob(jobId);

		assertTrue(jobTrue);
		assertTrue(articleTrue);

	}

	@Test
	void testUpdate() {
		Article article = getRandomArticle();
		articleService.create(article);

		Long articleId = article.getArticleId();
		article.setTitle("titleUpdated");
		articleService.update(article);

		Article articleGet = articleService.get(articleId);
		assertEquals("titleUpdated", articleGet.getTitle());
	}

	@Test
	void testUpdateFail() {
		Article article = getRandomArticle();
		article.setArticleId(articleService.count() + 1L);
		assertThrows(EntityNotFoundException.class, () -> {
			articleService.update(article);
		});
	}

	@Test
	void testUpdateWithJob() {

		Long count = articleService.count();

		Long jobId = RandomUtils.nextLong();
		while (jobRepository.exist(jobId)) {
			jobId = RandomUtils.nextLong();
		}

		Job job = new Job(jobId);
		jobRepository.create(job);

		Article article = articleService.get(1L);

		article.getJobs().add(job);
		job.getArticles().add(article);

		jobRepository.update(job);

		Article articleGet = articleService.get(1L);
		Job jobGet = jobRepository.get(jobId);

		assertTrue(jobGet.hasArticle(articleGet.getArticleId()));
		assertTrue(articleGet.hasJob(jobGet.getJobId()));

		assertEquals(count, articleService.count());
	}

	@Test
	void testExist() {
		Article article = articleService.get(1L);
		assertEquals(1L, articleService.exist(article));
	}

	@Test
	void testExistWithoutDoi() {
		Article article = getRandomArticle();
		article.setDoi("");
		articleService.create(article);
		assertEquals(article.getArticleId(), articleService.exist(article));
	}

	@Test
	void testDelete() {
		Long count = articleService.count();

		Article article = getRandomArticle();
		Job job = new Job(jobRepository.count() + 1L);

		articleService.create(article);
		jobRepository.create(job);

		article.getJobs().add(job);
		job.getArticles().add(article);

		jobRepository.update(job);

		assertEquals(count + 1L, articleService.count());

		articleService.delete(article.getArticleId());
		assertEquals(count, articleService.count());

	}

	@Test
	void testDeleteFail() {
		Long articleIdNotExist = articleService.count() + 1L;
		assertThrows(EntityNotFoundException.class, () -> {
			articleService.delete(articleIdNotExist);
		});
	}

	private Article getRandomArticle() {
		Article article = new Article();
		article.setDoi(RandomStringUtils.randomAlphanumeric(1, 100));
		article.setTitle(RandomStringUtils.randomAlphanumeric(1, 100));
		article.setArticleAbstract(RandomStringUtils.randomAlphanumeric(1, 100));
		article.setUrl(RandomStringUtils.randomAlphanumeric(1, 100));
		return article;
	}
}
