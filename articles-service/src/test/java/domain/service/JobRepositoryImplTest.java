/* (C)2022 */
package domain.service;

import domain.model.Job;
import domain.model.Article;
import java.util.List;

import javax.inject.Inject;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.EntityExistsException;

import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@QuarkusTest
class JobRepositoryImplTest {
	@Inject
	EntityManager em;

	@Inject
	ArticleService articleService;

	@Inject
	JobRepository jobRepository;

	@Test
	void testGet() {
		Job job = jobRepository.get(1L);
		assertEquals(1L, job.getJobId());
	}

	@Test
	void testGetFail() {
		Long jobIdNotExist = jobRepository.count() + 1L;
		assertThrows(EntityNotFoundException.class, () -> {
			jobRepository.get(jobIdNotExist);
		});
	}

	@Test
	void testGetAll() {
		Long count = jobRepository.count();
		List<Job> jobs = jobRepository.getAll();
		Long size = Long.valueOf(jobs.size());
		assertEquals(count, size);
	}

	@Test
	void testCreate() {
		Long count = jobRepository.count();

		Job job = new Job(findNextId());
		jobRepository.create(job);

		assertEquals(count + 1L, jobRepository.count());
	}

	@Test
	void testCreateFail() {
		Job job = new Job(findNextId());
		jobRepository.create(job);

		assertThrows(EntityExistsException.class, () -> {
			jobRepository.create(job);
		});
	}

	@Test
	void testUpdate() {
		Job job = new Job(findNextId());
		jobRepository.create(job);

		List<Article> articles = articleService.getAll();

		job.getArticles().add(articles.get(0));
		articles.get(0).getJobs().add(job);

		jobRepository.update(job);

		Job jobGet = jobRepository.get(job.getJobId());

		assertTrue(jobGet.hasArticle(articles.get(0).getArticleId()));
		assertTrue(articles.get(0).hasJob(jobGet.getJobId()));
	}

	@Test
	void testUpdateFail() {
		Job job = new Job(findNextId());
		assertThrows(EntityNotFoundException.class, () -> {
			jobRepository.update(job);
		});
	}

	@Test
	void testDelete() {

		Long count = jobRepository.count();

		Job job = new Job(findNextId());

		jobRepository.create(job);

		jobRepository.delete(job.getJobId());

		assertEquals(count, jobRepository.count());
	}

	@Test
	void testDeleteWithArticle() {

		Long count = jobRepository.count();

		Job job = new Job(findNextId());
		jobRepository.create(job);

		List<Article> articles = articleService.getAll();
		articles.get(0).getJobs().add(job);
		job.getArticles().add(articles.get(0));

		jobRepository.update(job);

		jobRepository.delete(job.getJobId());

		assertEquals(count, jobRepository.count());
	}

	@Test
	void testDeleteFail() {
		Long jobIdNotExist = findNextId();
		assertThrows(EntityNotFoundException.class, () -> {
			jobRepository.delete(jobIdNotExist);
		});
	}

	@Test
	void testExist() {
		assertTrue(jobRepository.exist(1L));
	}

	private Long findNextId() {
		Long count = jobRepository.count();
		Long jobId = count + 1L;
		while (jobRepository.exist(jobId)) {
			jobId++;
		}
		return jobId;
	}

}
