/* (C)2022 */
package domain.model;

import org.junit.jupiter.api.Test;
import io.quarkus.test.junit.QuarkusTest;

import static org.junit.jupiter.api.Assertions.assertTrue;

@QuarkusTest
class JobTest {

	@Test
	void testArticle() {
		Job job = new Job();
		assertTrue(Job.class.isInstance(job));
	}

	@Test
	void testArticleWithAllArgs() {
		Job job = new Job(1L);
		assertTrue(Job.class.isInstance(job));
	}

}
