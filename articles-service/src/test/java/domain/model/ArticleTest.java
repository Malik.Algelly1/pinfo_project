/* (C)2022 */
package domain.model;

import org.junit.jupiter.api.Test;
import io.quarkus.test.junit.QuarkusTest;

import static org.junit.jupiter.api.Assertions.assertTrue;

@QuarkusTest
class ArticleTest {

	@Test
	void testArticle() {
		Article article = new Article();
		article.setArticleId(0L);
		article.setDoi("doi");
		article.setTitle("title");
		article.setArticleAbstract("articleAbstract");
		article.setUrl("url");
		assertTrue(Article.class.isInstance(article));
	}

	@Test
	void testArticleWithAllArgs() {

		Article article = new Article("doi", "title", "abstract", "url");
		article.getArticleId();
		article.getDoi();
		article.getTitle();
		article.getArticleAbstract();
		article.getUrl();
		assertTrue(Article.class.isInstance(article));
	}

}
