/* (C)2022 */
package api.rest;

import java.util.Set;

import java.util.HashSet;
import java.util.Arrays;

import org.junit.jupiter.api.Test;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.oidc.server.OidcWiremockTestResource;
import io.restassured.http.ContentType;
import io.smallrye.jwt.build.Jwt;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.containsString;

@QuarkusTest
@QuarkusTestResource(OidcWiremockTestResource.class)
class ArticleRestServiceTest {

	@Test
	void testAuthNotOk() {
		given().auth().oauth2("expired")
				.when().get("/articles")
				.then()
				.statusCode(401);
	}

	@Test
	void testGet() {
		given().auth().oauth2(getAccessToken(new HashSet<>(Arrays.asList("user"))))
				.when().get("/articles/1")
				.then()
				.statusCode(200)
				.body(containsString("Title1"));
	}

	@Test
	void testGetFail() {
		given().auth().oauth2(getAccessToken(new HashSet<>(Arrays.asList("user"))))
				.when().get("/articles/9999")
				.then()
				.statusCode(404);
	}

	@Test
	void testCount() {
		given().auth().oauth2(getAccessToken(new HashSet<>(Arrays.asList("user"))))
				.when().get("/articles/count")
				.then().statusCode(200)
				.body(is("4"));
	}

	@Test
	void testGetByList() {
		Long[] toSend = { 1L, 2L, 3L };
		given().contentType(ContentType.JSON)
				.auth().oauth2(getAccessToken(new HashSet<>(Arrays.asList("user"))))
				.body(toSend)
				.when().get("/articles")
				.then().statusCode(200)
				.body(containsString("Title1"));
	}

	private String getAccessToken(Set<String> groups) {
		return Jwt.groups(groups)
				.issuer("https://server.example.com")
				.audience("https://service.example.com")
				.sign();
	}

}
