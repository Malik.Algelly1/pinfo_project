/* (C)2022 */
package api.msg;

import java.util.HashMap;
import java.util.Map;

import io.quarkus.test.common.QuarkusTestResourceLifecycleManager;
import io.smallrye.reactive.messaging.providers.connectors.InMemoryConnector;

public class KafkaTestResourceLifecycleManager implements QuarkusTestResourceLifecycleManager {

	@Override
	public Map<String, String> start() {
		Map<String, String> env = new HashMap<>();
		Map<String, String> props = InMemoryConnector.switchIncomingChannelsToInMemory("articles");
		env.putAll(props);
		return env;
	}

	@Override
	public void stop() {
		InMemoryConnector.clear();
	}
}
