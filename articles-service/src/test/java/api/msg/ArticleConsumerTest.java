/* (C)2022 */
package api.msg;

import domain.model.ArticleReception;
import domain.service.ArticleService;
import domain.service.JobRepository;

import org.junit.jupiter.api.Test;

import javax.enterprise.inject.Any;
import javax.inject.Inject;

import static org.mockito.Mockito.when;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;

import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import io.quarkus.test.oidc.server.OidcWiremockTestResource;
import io.smallrye.reactive.messaging.providers.connectors.InMemoryConnector;

@QuarkusTest
@QuarkusTestResource(KafkaTestResourceLifecycleManager.class)
@QuarkusTestResource(OidcWiremockTestResource.class)
class ArticleConsumerTest {

	@Inject
	@Any
	InMemoryConnector connector;

	@InjectMock
	ArticleService articleService;

	@InjectMock
	JobRepository jobRepository;

	@Inject
	ArticleConsumer consumer;

	@Test
	void testAddData() {

		Long jobId = RandomUtils.nextLong();
		while (jobRepository.exist(jobId)) {
			jobId = RandomUtils.nextLong();
		}

		for (int i = 0; i < 10; i++) {
			consumer.addData(getRandomArticleReception());
		}
	}

	@Test
	void testAddDataAlreadyExist() {

		Long count = articleService.count();

		Long jobId = RandomUtils.nextLong();
		while (jobRepository.exist(jobId)) {
			jobId = RandomUtils.nextLong();
		}

		ArticleReception articleR = new ArticleReception("doi1", "Title1", "Abstract1", "Url1", 0L);

		consumer.addData(articleR);
		when(articleService.count()).thenReturn(count);
	}

	private ArticleReception getRandomArticleReception() {
		ArticleReception articleR = new ArticleReception();
		articleR.setDoi(RandomStringUtils.randomAlphanumeric(1, 100));
		articleR.setTitle(RandomStringUtils.randomAlphanumeric(1, 100));
		articleR.setArticleAbstract(RandomStringUtils.randomAlphanumeric(1, 100));
		articleR.setUrl(RandomStringUtils.randomAlphanumeric(1, 100));
		return articleR;
	}
}
