/* (C)2022 */
package api.rest;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsString;

import java.util.List;

import javax.inject.Inject;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Test;

import domain.model.Search;
import domain.model.SearchRequest;
import domain.service.SearchService;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.oidc.server.OidcWiremockTestResource;
import io.restassured.http.ContentType;
import io.smallrye.jwt.build.Jwt;

@QuarkusTest
@QuarkusTestResource(OidcWiremockTestResource.class)
class SearchServiceRestTest {

	@Inject
	SearchService searchService;

	@Test
	void testAuthNotOk() {
		given().auth().oauth2("expired")
				.when().get("/searches")
				.then()
				.statusCode(HttpStatus.SC_UNAUTHORIZED);
	}

	@Test
	void testGetAll() {
		given().contentType(ContentType.JSON).auth().oauth2(getAccessToken("200")).body(getRandomSearch())
				.when().get("/searches")
				.then()
				.statusCode(HttpStatus.SC_OK)
				.body(containsString("test0"));

	}

	@Test
	void testGetAllFail() {
		given().contentType(ContentType.JSON).auth().oauth2(getAccessToken("1000")).body(getRandomSearch())
				.when().get("/searches")
				.then()
				.statusCode(HttpStatus.SC_OK);

	}

	@Test
	void testGet() {
		given().contentType(ContentType.JSON).auth().oauth2(getAccessToken("200")).body(getRandomSearch())
				.when().get("/searches/1")
				.then()
				.statusCode(HttpStatus.SC_OK)
				.body(containsString("test0"));

	}

	@Test
	void testGetFail() {
		given().contentType(ContentType.JSON).auth().oauth2(getAccessToken("1000")).body(getRandomSearch())
				.when().get("/searches/1")
				.then()
				.statusCode(HttpStatus.SC_OK);
	}

	@Test
	void testPost() {
		given().contentType(ContentType.JSON).auth().oauth2(getAccessToken("209")).body(getRandomSearch())
				.when().post("/searches")
				.then()
				.statusCode(HttpStatus.SC_CREATED);

	}

	@Test
	void testDelete() {
		SearchRequest search = new SearchRequest();
		search.setUserId("208");
		search.setEmail(RandomStringUtils.randomAlphanumeric(1, 50));
		search.setKeyWord(RandomStringUtils.randomAlphanumeric(1, 50));
		searchService.createSearch(search);
		List<Search> before = searchService.getSearchsByUserId("208");
		Long searchId = before.get(0).getSearchId();
		String searchIdFormat = Long.toString(searchId);

		given().auth().oauth2(getAccessToken("208"))
				.when().delete("/searches/" + searchIdFormat)
				.then()
				.statusCode(HttpStatus.SC_OK);

	}

	public SearchRequest getRandomSearch() {
		SearchRequest searchRequest = new SearchRequest();
		searchRequest.setUserId(RandomStringUtils.randomAlphanumeric(1, 50));
		searchRequest.setEmail(RandomStringUtils.randomAlphanumeric(1, 50));
		searchRequest.setKeyWord(RandomStringUtils.randomAlphanumeric(1, 50));
		return searchRequest;
	}

	public String getAccessToken(String subject) {
		return Jwt.preferredUserName("Hello")
				.groups("user")
				.subject(subject)
				.issuer("https://server.example.com")
				.audience("https://service.example.com")
				.sign();
	}

}
