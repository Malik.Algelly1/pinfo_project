/* (C)2022 */
package api.msg;

import java.util.HashMap;
import java.util.Map;

import io.quarkus.test.common.QuarkusTestResourceLifecycleManager;
import io.smallrye.reactive.messaging.providers.connectors.InMemoryConnector;

public class KafkaTestResourceLifecycleManager implements
		QuarkusTestResourceLifecycleManager {

	@Override
	public Map<String, String> start() {
		Map<String, String> env = new HashMap<>();
		// Map<String, String> props1 =
		// InMemoryConnector.switchIncomingChannelsToInMemory("saveNewJob");
		Map<String, String> props2 = InMemoryConnector.switchIncomingChannelsToInMemory("updateJob");
		Map<String, String> props3 = InMemoryConnector.switchOutgoingChannelsToInMemory("createJob");
		Map<String, String> props4 = InMemoryConnector.switchOutgoingChannelsToInMemory("deleteJob");
		// env.putAll(props1);
		env.putAll(props2);
		env.putAll(props3);
		env.putAll(props4);
		return env;
	}

	@Override
	public void stop() {
		InMemoryConnector.clear();
	}
}
