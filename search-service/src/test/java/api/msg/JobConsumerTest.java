/* (C)2022 */
package api.msg;

import static org.junit.jupiter.api.Assertions.assertTrue;

import javax.enterprise.inject.Any;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.junit.jupiter.api.Test;

import domain.model.SearchComm;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;

import io.quarkus.test.oidc.server.OidcWiremockTestResource;
import io.smallrye.reactive.messaging.providers.connectors.InMemoryConnector;
import io.smallrye.reactive.messaging.providers.connectors.InMemorySink;

@QuarkusTest
@QuarkusTestResource(KafkaTestResourceLifecycleManager.class)
@QuarkusTestResource(OidcWiremockTestResource.class)
class JobConsumerTest {

	@Inject
	@Any
	InMemoryConnector connector;

	@Inject
	EntityManager em;

	@Inject
	JobConsumer consumer;

	@Test
	void testCreateJob() {
		InMemorySink<SearchComm> instrumentsOut = connector.sink("createJob");

		consumer.createJob(0L, "keyWord");

		assertTrue(instrumentsOut.received().size() > 0);
	}

	@Test
	void testDeleteJob() {
		InMemorySink<Long> instrumentsOut = connector.sink("deleteJob");

		consumer.deleteJob(0L);

		assertTrue(instrumentsOut.received().size() > 0);

	}
}
