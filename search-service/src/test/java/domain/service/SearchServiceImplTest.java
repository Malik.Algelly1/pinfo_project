/* (C)2022 */
package domain.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import javax.inject.Inject;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;

import domain.model.Search;
import domain.model.SearchRequest;
import io.quarkus.mailer.Mail;
import io.quarkus.mailer.MockMailbox;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
class SearchServiceImplTest {

	@Inject
	EntityManager em;

	@Inject
	SearchService searchService;

	@Inject
	MockMailbox mailbox;

	@Test
	void testGetSearch() {
		Search search = searchService.getSearch(1L);
		assertEquals("test0", search.getSearchRequest().getKeyWord());

	}

	@Test
	void testGetSearchFail() {
		assertThrows(EntityNotFoundException.class, () -> {
			searchService.getSearch(1000L);
		});
	}

	@Test
	void testGetSearchsByUserId() {
		List<Search> search = searchService.getSearchsByUserId("200");
		assertEquals(2, search.size());
	}

	@Test
	void testGetSearchsByUserIdFail() {
		assertThrows(EntityNotFoundException.class, () -> {
			searchService.getSearchsByUserId("1000");
		});
	}

	@Test
	void testGetSearchsByJobId() {
		List<Search> search = searchService.getSearchsByJobId(100L);
		assertEquals(2, search.size());
	}

	@Test
	void testGetSearchsByJobIdFail() {
		assertThrows(EntityNotFoundException.class, () -> {
			searchService.getSearchsByJobId(1000L);
		});
	}

	@Test
	void testCreateSearch() {
		SearchRequest search = getRandomSearch("207");
		searchService.createSearch(search);
		assertEquals(1, searchService.getSearchsByUserId("207").size());
	}

	@Test
	void testDeleteSearch() {
		SearchRequest search = getRandomSearch("208");
		searchService.createSearch(search);
		List<Search> toDelete = searchService.getSearchsByUserId("208");

		for (Search s : toDelete) {
			searchService.deleteSearch(s);
		}

		assertThrows(EntityNotFoundException.class, () -> {
			searchService.getSearchsByUserId("208");
		});
	}

	@Test
	void testNewJobId() {
		searchService.newJobId(6L, 104L, "IN_PROGRESS");
		Search search = em.find(Search.class, 6L);
		assertEquals(104L, search.getJobId());
	}

	@Test
	void testNewJobIdFail() {
		assertThrows(EntityNotFoundException.class, () -> {
			searchService.newJobId(1000L, 1000L, "IN_PROGRESS");
		});
	}

	@Test
	void testUpdateJobStatus() {
		searchService.updateJobStatus(105L, "FINISHED");
		Search search = em.find(Search.class, 7L);
		assertEquals("FINISHED", search.getJobStatus());
	}

	@Test
	@Transactional
	void testEndJobMailer() {

		SearchRequest searchRequest = new SearchRequest();
		Search search = new Search();

		searchRequest.setUserId("209");
		searchRequest.setEmail("test@quarkus.io");
		searchRequest.setKeyWord("");

		search.setJobId(Long.valueOf(10));
		search.setSearchDate(LocalDate.now());
		search.setSearchRequest(searchRequest);

		em.persist(search);

		mailbox.clear();

		searchService.endJobMailer(Long.valueOf(10));

		List<Mail> sent = mailbox.getMessagesSentTo("test@quarkus.io");
		assertEquals(sent.size(), 1);
		Mail actual = sent.get(0);
		assertTrue(actual.getSubject().contains("SRAS SEARCH NO : " + Long.toString(search.getSearchId())));
		assertTrue(actual.getText().contains("The search " + Long.toString(search.getSearchId())
				+ " is finished, the results are now available on the website.\n\nBest regards, the SRAS team."));

	}

	public SearchRequest getRandomSearch(String userId) {
		SearchRequest searchRequest = new SearchRequest();
		searchRequest.setUserId(userId);
		searchRequest.setEmail(RandomStringUtils.randomAlphanumeric(1, 50));
		searchRequest.setKeyWord(RandomStringUtils.randomAlphanumeric(1, 50));
		return searchRequest;
	}

}
