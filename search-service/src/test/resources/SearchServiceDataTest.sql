drop table Search if exists;
drop sequence if exists SEARCH_SEQ;

create sequence SEARCH_SEQ start with 1 increment by 1;

create table Search(
    searchId Bigint not null,
    jobId Bigint,
    jobStatus varchar(255),
    searchDate Date,
    userId varchar(255),
    email Varchar(255),
    keyWord varchar(255),
    primary key(searchId)
);

INSERT INTO Search(searchId,jobId,jobStatus,searchDate,userId,email,keyWord) VALUES (SEARCH_SEQ.nextval,100,'FINISHED',PARSEDATETIME('23-02-2022','yyyy-dd-mm','en'),'200','mail@test0','test0');
INSERT INTO Search(searchId,jobId,jobStatus,searchDate,userId,email,keyWord) VALUES (SEARCH_SEQ.nextval,101,'FINISHED',PARSEDATETIME('24-02-2022','yyyy-dd-mm','en'),'200','mail@test0','test0');
INSERT INTO Search(searchId,jobId,jobStatus,searchDate,userId,email,keyWord) VALUES (SEARCH_SEQ.nextval,100,'FINISHED',PARSEDATETIME('25-02-2022','yyyy-dd-mm','en'),'201','mail@test1','test1');
INSERT INTO Search(searchId,jobId,jobStatus,searchDate,userId,email,keyWord) VALUES (SEARCH_SEQ.nextval,102,'FINISHED',PARSEDATETIME('26-02-2022','yyyy-dd-mm','en'),'202','mail@test2','test2');
INSERT INTO Search(searchId,jobId,jobStatus,searchDate,userId,email,keyWord) VALUES (SEARCH_SEQ.nextval,103,'FINISHED',PARSEDATETIME('27-02-2022','yyyy-dd-mm','en'),'203','mail@test3','test3');
INSERT INTO Search(searchId,jobId,jobStatus,searchDate,userId,email,keyWord) VALUES (SEARCH_SEQ.nextval,-1,'IN_PROGRESS',PARSEDATETIME('28-02-2022','yyyy-dd-mm','en'),'204','mail@test4','test4');
INSERT INTO Search(searchId,jobId,jobStatus,searchDate,userId,email,keyWord) VALUES (SEARCH_SEQ.nextval,105,'IN_PROGRESS',PARSEDATETIME('29-02-2022','yyyy-dd-mm','en'),'205','mail@test5','test5');
INSERT INTO Search(searchId,jobId,jobStatus,searchDate,userId,email,keyWord) VALUES (SEARCH_SEQ.nextval,106,'IN_PROGRESS',PARSEDATETIME('30-02-2022','yyyy-dd-mm','en'),'206','mail@test6','test6');