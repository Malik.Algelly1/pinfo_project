/* (C)2022 */
package api.msg;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;

import io.smallrye.reactive.messaging.annotations.Blocking;

import domain.model.SearchComm;
import domain.service.SearchService;

@ApplicationScoped
public class JobConsumer {

	@Inject
	SearchService searchService;

	@Inject
	@Channel("createJob")
	Emitter<SearchComm> newJobEmitter;

	@Inject
	@Channel("deleteJob")
	Emitter<Long> delJobEmitter;

	public void createJob(Long searchId, String keyWord) {
		SearchComm searchComm = new SearchComm();
		searchComm.setSearchId(searchId);
		searchComm.setKeyWord(keyWord);
		newJobEmitter.send(searchComm);
	}

	public void deleteJob(Long jobId) {
		delJobEmitter.send(jobId);
	}

	// @Incoming("saveNewJob")
	// @Blocking
	// public void newJobId(SearchComm toSave) {
	// System.out.println("in newJobId");
	// searchService.newJobId(toSave.getSearchId(), toSave.getJobId(),
	// toSave.getJobStatus());
	// }

	@Incoming("updateJob")
	@Blocking
	public void uptadeJobsStatus(SearchComm toUpdate) {
		System.out.println("in updateJobStatus");
		if (toUpdate.getJobStatus().equals("FINISHED")) {
			searchService.updateJobStatusBySearchId(toUpdate.getSearchId(), toUpdate.getJobId(),
					toUpdate.getJobStatus());
			searchService.endJobMailer(toUpdate.getJobId());
		} else {
			searchService.updateJobStatusBySearchId(toUpdate.getSearchId(), toUpdate.getJobId(),
					toUpdate.getJobStatus());
		}
	}

}
