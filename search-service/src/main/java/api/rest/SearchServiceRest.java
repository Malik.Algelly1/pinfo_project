/* (C)2022 */
package api.rest;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.security.PermitAll;
import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.Produces;
import javax.inject.Inject;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import domain.model.Search;
import domain.model.SearchRequest;
import domain.service.SearchService;
import api.msg.JobConsumer;
import org.eclipse.microprofile.jwt.JsonWebToken;

@ApplicationScoped
@Path("/searches")
public class SearchServiceRest {

	@Inject
	SearchService searchService;

	@Inject
	JsonWebToken jwt;

	@Inject
	JobConsumer consumer;

	@GET
	@PermitAll
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllSearchs() {
		String userId = jwt.getSubject();
		List<Search> searches = new ArrayList<Search>();
		try {
			searches = searchService.getSearchsByUserId(userId);
		} catch (EntityNotFoundException e) {
			return Response.ok().build();
		}

		return Response.ok(searches).build();
	}

	@GET
	@PermitAll
	@Path("{searchId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getSearchId(@PathParam("searchId") Long searchId) {
		// String userId = jwt.getSubject();
		Search search = new Search();

		try {
			search = searchService.getSearch(searchId);
		} catch (NoResultException e) {
			return Response.ok().build();
		}

		return Response.ok(search).build();
	}

	@POST
	@PermitAll
	@Consumes(MediaType.APPLICATION_JSON)
	public Response create(SearchRequest searchRequest) {
		System.out.println("Subject: ");
		System.out.println(jwt.getSubject());
		searchRequest.setUserId(jwt.getSubject());
		Search search = searchService.createSearch(searchRequest);

		consumer.createJob(search.getSearchId(), search.getSearchRequest().getKeyWord());

		URI ressourceUri = UriBuilder.fromResource(SearchServiceRest.class).path("{searchId}")
				.build(search.getSearchId());
		return Response.created(ressourceUri).build();

	}

	@DELETE
	@PermitAll
	@Path("{searchId}")
	public Response delete(@PathParam("searchId") Long searchId) {
		// String userId = jwt.getSubject();

		Search search = searchService.getSearch(searchId);
		consumer.deleteJob(search.getJobId());

		searchService.deleteSearch(search);
		return Response.ok().build();
	}
}
