/* (C)2022 */
package domain.service;

import java.util.List;

import domain.model.Search;
import domain.model.SearchRequest;

public interface SearchService {

	Search getSearch(Long searchId);

	List<Search> getSearchsByUserId(String userId);

	List<Search> getSearchsByJobId(Long jobId);

	Search createSearch(SearchRequest search);

	void deleteSearch(Search search);

	void newJobId(Long search, Long jobId, String jobStatus);

	void updateJobStatus(Long jobId, String jobStatus);

	void updateJobStatusBySearchId(Long searchId, Long jobId, String jobStatus);

	void endJobMailer(Long jobId);

}
