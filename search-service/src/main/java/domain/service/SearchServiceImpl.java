/* (C)2022 */
package domain.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import domain.model.Search;
import domain.model.SearchRequest;
import io.quarkus.mailer.Mail;
import io.quarkus.mailer.Mailer;

@ApplicationScoped
public class SearchServiceImpl implements SearchService {

	@Inject
	EntityManager em;

	@Inject
	Mailer mailer;

	@Override
	public Search getSearch(Long searchId) {
		Search search = em.find(Search.class, searchId);
		if (Objects.nonNull(search)) {
			return search;
		} else {
			throw new EntityNotFoundException(
					"Not in database: Failed to get the search id: " + searchId);
		}
	}

	@Override
	public List<Search> getSearchsByUserId(String userId) {
		CriteriaBuilder builder = em.getCriteriaBuilder();
		CriteriaQuery<Search> criteria = builder.createQuery(Search.class);
		Root<Search> postroot = criteria.from(Search.class);
		criteria.where(builder.equal(builder.treat(postroot.get("searchRequest"), Search.class).get("userId"), userId));

		List<Search> searches = em.createQuery(criteria).getResultList();

		if (searches.isEmpty()) {
			throw new EntityNotFoundException("Failed to find searches from userId : " + userId);
		} else {
			return searches;
		}
	}

	@Override
	public List<Search> getSearchsByJobId(Long jobId) {
		CriteriaBuilder builder = em.getCriteriaBuilder();
		CriteriaQuery<Search> criteria = builder.createQuery(Search.class);
		Root<Search> postroot = criteria.from(Search.class);
		criteria.where(builder.equal(postroot.get("jobId"), jobId));

		List<Search> searches = em.createQuery(criteria).getResultList();

		if (searches.isEmpty()) {
			throw new EntityNotFoundException("Failed to find searches from jobId : " + jobId);
		} else {
			return searches;
		}
	}

	@Override
	@Transactional
	public Search createSearch(SearchRequest searchRequest) {
		Search search = new Search();
		search.setJobId(Long.valueOf(-1));
		search.setSearchDate(LocalDate.now());
		search.setSearchRequest(searchRequest);
		search.setJobStatus("IN_PROGRESS");
		em.persist(search);
		return search;
	}

	@Override
	@Transactional
	public void deleteSearch(Search search) {
		Search toDelete = em.find(Search.class, search.getSearchId());
		em.remove(toDelete);
	}

	@Override
	@Transactional
	public void newJobId(Long searchId, Long jobId, String jobStatus) {
		Search search = em.find(Search.class, searchId);
		if (Objects.nonNull(search)) {
			search.setJobId(jobId);
			search.setJobStatus(jobStatus);
			em.merge(search);
		} else {
			throw new EntityNotFoundException(
					"Failed to save newJobId, searchId (" + searchId + ") not found in database.");
		}
	}

	@Override
	@Transactional
	public void updateJobStatus(Long jobId, String jobStatus) {
		List<Search> searches = getSearchsByJobId(jobId);
		for (Search search : searches) {
			search.setJobStatus(jobStatus);
			em.merge(search);
		}
	}

	@Override
	@Transactional
	public void updateJobStatusBySearchId(Long searchId, Long jobId, String jobStatus) {
		Search search = getSearch(searchId);
		search.setJobStatus(jobStatus);
		if (jobId > 0) {
			search.setJobId(jobId);
		}
		em.merge(search);
	}

	@Override
	public void endJobMailer(Long jobId) {
		List<Search> searches = getSearchsByJobId(jobId);
		for (Search search : searches) {
			mailer.send(Mail.withText(String.valueOf(search.getSearchRequest().getEmail()),
					"SRAS SEARCH NO : " + Long.toString(search.getSearchId()),
					"The search " + Long.toString(search.getSearchId())
							+ " is finished, the results are now available on the website.\n\nBest regards, the SRAS team."));
		}
	}

}
