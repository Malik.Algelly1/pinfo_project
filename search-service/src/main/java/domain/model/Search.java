/* (C)2022 */
package domain.model;

import java.time.LocalDate;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
@Entity
public class Search {
	@Id
	@SequenceGenerator(name = "SEARCH_SEQ", sequenceName = "SEARCH_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEARCH_SEQ")
	private Long searchId;
	@NotNull
	private Long jobId;
	@NotNull
	public String jobStatus;
	@NotNull
	private LocalDate searchDate;
	@Embedded
	public SearchRequest searchRequest;
}
