/* (C)2022 */
package domain.model;

import io.quarkus.kafka.client.serialization.ObjectMapperDeserializer;

public class SearchCommDeserializer extends ObjectMapperDeserializer<SearchComm> {
	public SearchCommDeserializer() {
		super(SearchComm.class);
	}
}
