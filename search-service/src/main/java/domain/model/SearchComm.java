/* (C)2022 */
package domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SearchComm {
	private Long searchId;
	private Long jobId;
	private String jobStatus;
	private String keyWord;
}
