/* (C)2022 */
package domain.model;

import javax.persistence.Embeddable;

import io.smallrye.common.constraint.NotNull;
import lombok.Data;

@Data
@Embeddable
public class SearchRequest {

	@NotNull
	private String userId;
	@NotNull
	private String email;
	@NotNull
	private String keyWord;

}
