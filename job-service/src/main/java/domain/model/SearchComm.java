/* (C)2022 */
package domain.model;

import lombok.Data;

@Data
public class SearchComm {
	Long searchId;
	Long jobId;
	String jobStatus;
	String keyWord;
}
