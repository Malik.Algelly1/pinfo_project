/* (C)2022 */
package domain.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@Entity
public class Job {
	@Id
	@SequenceGenerator(name = "JOB_SEQ", sequenceName = "JOB_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "JOB_SEQ")
	private Long jobId;

	@NotNull
	private String dnfQuery;

	@NotNull
	private String jobStatus; // FINISHED, IN_PROGRESS, CANCELLED, ERROR

	@NotNull
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private LocalDate createDate;

	@NotNull
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private LocalDate accessDate;

	@NotNull
	private Long counter;
}
