/* (C)2022 */
package domain.model;

import io.quarkus.kafka.client.serialization.ObjectMapperSerializer;

public class SearchCommSerializer extends ObjectMapperSerializer<SearchComm> {
	public SearchCommSerializer() {
		super();
	}
}
