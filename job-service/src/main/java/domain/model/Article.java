/* (C)2022 */
package domain.model;

import lombok.Data;

@Data
public class Article {
	public Article(Long jobId, String doi, String title, String articleAbstract, String url) {
		this.jobId = jobId;
		this.doi = doi;
		this.title = title;
		this.articleAbstract = articleAbstract;
		this.url = url;
	}

	private Long jobId;
	private String doi;
	private String title;
	private String articleAbstract;
	private String url;

}
