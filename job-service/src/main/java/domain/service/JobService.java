/* (C)2022 */
package domain.service;

import domain.model.Job;

public interface JobService {

	boolean exists(Long jobId);

	boolean exists(String query);

	void create(Job job);

	void update(Job job, Boolean newSearch);

	void delete(Long jobId);

	Job get(Long jobId);

	Job get(String query);

	Long count();

	Long count(Long jobId);

	Long count(String jobStatus);

	String launch(Job job); // send to Baobab

}
