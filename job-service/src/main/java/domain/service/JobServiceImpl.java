/* (C)2022 */
package domain.service;

import aima.core.logic.propositional.parsing.PLParser;
import aima.core.logic.propositional.parsing.ast.Sentence;
import aima.core.logic.propositional.visitors.ConvertToDNF;

import domain.model.Job;
import lombok.extern.java.Log;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.Objects;
import java.util.HashMap;
import java.util.List;
import java.util.StringJoiner;
import java.time.LocalDate;

@ApplicationScoped
@Log
public class JobServiceImpl implements JobService {

	@Inject
	EntityManager em;

	final private String jobIdError;
	final private String doesntExistError;
	final private String alreadyExistError;

	public JobServiceImpl() {
		jobIdError = "Error: job id ";
		alreadyExistError = " already exists.";
		doesntExistError = " does not exist.";
	}

	public JobServiceImpl(EntityManager em) {
		this();
		this.em = em;
	}

	@Override
	@Transactional
	public void create(Job job) {
		if (job.getJobId() == null)
			em.persist(job);
		else
			throw new EntityExistsException(jobIdError + job.getJobId() + alreadyExistError);
	}

	@Override
	@Transactional
	public void update(Job job, Boolean newSearch) {
		Job jobInDB = em.find(Job.class, job.getJobId());
		if (Objects.nonNull(jobInDB)) {
			job.setCreateDate(job.getCreateDate());
			job.setAccessDate(LocalDate.now());
			if (newSearch)
				job.setCounter(job.getCounter() + 1L);
			em.merge(job);
		} else
			throw new NoResultException(jobIdError + job.getJobId() + doesntExistError);
	}

	@Override
	@Transactional
	public void delete(Long jobId) {
		Job jobInDB = em.find(Job.class, jobId);
		if (Objects.nonNull(jobInDB))
			em.remove(jobInDB);
		else
			throw new NoResultException(jobIdError + jobId + doesntExistError);
	}

	@Override
	public Job get(Long jobId) {
		Job jobInDB = em.find(Job.class, jobId);
		if (Objects.nonNull(jobInDB))
			return jobInDB;
		else
			throw new NoResultException(jobIdError + jobId + doesntExistError);
	}

	@Override
	public Job get(String dnfQuery) {
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Job> criteriaQuery = criteriaBuilder.createQuery(Job.class);
		Root<Job> root = criteriaQuery.from(Job.class);
		criteriaQuery.select(root);
		criteriaQuery.where(criteriaBuilder.equal(root.get("dnfQuery"), dnfQuery));
		try {
			return em.createQuery(criteriaQuery).getSingleResult();
		} catch (IllegalArgumentException | NoResultException e) {
			throw new NoResultException("Error: query " + dnfQuery + doesntExistError);
		}
	}

	@Override
	public boolean exists(Long jobId) {
		return Objects.nonNull(em.find(Job.class, jobId));
	}

	@Override
	public boolean exists(String query) {
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Job> criteriaQuery = criteriaBuilder.createQuery(Job.class);
		Root<Job> root = criteriaQuery.from(Job.class);
		criteriaQuery.select(root).where(criteriaBuilder.equal(root.get("dnfQuery"), query));
		try {
			Job jobInDB = em.createQuery(criteriaQuery).getSingleResult();
			return Objects.nonNull(jobInDB);
		} catch (NoResultException e) {
			return false;
		}
	}

	@Override
	public Long count() {
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
		criteriaQuery.select(criteriaBuilder.count(criteriaQuery.from(Job.class)));
		return em.createQuery(criteriaQuery).getSingleResult();
	}

	@Override
	public Long count(Long jobId) {
		Job jobInDB = em.find(Job.class, jobId);
		if (Objects.nonNull(jobInDB))
			return jobInDB.getCounter();
		else
			throw new EntityNotFoundException(jobIdError + jobId + doesntExistError);
	}

	@Override
	public Long count(String jobStatus) {
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
		Root<Job> root = criteriaQuery.from(Job.class);
		criteriaQuery.select(criteriaBuilder.count(root));
		criteriaQuery.where(criteriaBuilder.equal(root.get("jobStatus"), jobStatus));
		return em.createQuery(criteriaQuery).getSingleResult();
	}

	@Override
	public String launch(Job job) {
		log.info("Working Directory = " + System.getProperty("user.dir"));
		// Launch quartz job using quartz
		// csvPath
		return System.getProperty("user.dir") + "/src/test/resources/sample.csv";
	}

	public static String normalizeQuery(String query) {
		log.info("Got query: " + query);
		String repl = "" + (char) 190;
		int w = 0;
		int i;
		List<String> keywords = List.of("and", "&", "&&", "or", "|", "||", "not", "~", "!");
		StringJoiner ll = new StringJoiner(" ");

		HashMap<String, String> hm = new HashMap<>();

		query = query.replace(repl, "");
		query = query.replace("_", repl);

		query = query.replace("&&", "&");
		query = query.replace("&", " & ");
		query = query.replace("||", "|");
		query = query.replace("|", " | ");
		query = query.replace("!", " ! ");
		query = query.replace("~", " ~ ");

		query = query.replaceAll("\\s{2,}", " ").trim();
		String[] words = query.split("\\s");

		for (i = 0; i < words.length; ++i) {
			String word = words[i].toLowerCase();
			if (w == 0) {
				switch (word) {
					case "and", "&", "&&" -> ll.add("&");
					case "or", "|", "||" -> ll.add("|");
					case "not", "~", "!" -> ll.add("~");
					default -> w++;
				}
			} else {
				if (!keywords.contains(word)) {
					++w;
					continue;
				}
				combineString(w, i, ll, hm, words);
				--i;
				w = 0;
			}
		}
		if (w != 0) {
			combineString(w, i, ll, hm, words);
		}
		PLParser parser = new PLParser();
		Sentence parsed = parser.parse(ll.toString());
		Sentence normalized = ConvertToDNF.convert(parsed);
		String newQuery = normalized.toString();
		newQuery = newQuery.replace("(", "( ");
		newQuery = newQuery.replace(")", " )");
		String[] newWords = newQuery.split("\\s");
		StringJoiner normalizedQuery = new StringJoiner(" ");
		for (String token : newWords)
			normalizedQuery.add(hm.getOrDefault(token, token).replace(repl, "_"));
		String dnfQuery = normalizedQuery
				.toString()
				.replace("~", "~ ")
				.replaceAll("\\s{2,}", " ")
				.trim();
		log.info(dnfQuery);
		return dnfQuery;
	}

	private static void combineString(int w, int i, StringJoiner ll, HashMap<String, String> hm, String[] words) {
		StringJoiner s;
		s = new StringJoiner(" ");
		for (int j = i - w; j < i; ++j)
			s.add(words[j]);
		String value = s.toString();
		String key = value.replace(" ", "_");
		ll.add(key);
		hm.put(key, value);
	}

}
