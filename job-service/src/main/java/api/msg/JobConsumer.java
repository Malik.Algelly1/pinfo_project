/* (C)2022 */
package api.msg;

import domain.model.Article;
import domain.model.Job;
import domain.model.SearchComm;
import domain.service.JobService;

import java.time.LocalDate;
import java.util.Objects;

import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;
import org.eclipse.microprofile.reactive.messaging.Incoming;

import io.smallrye.reactive.messaging.annotations.Blocking;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.NoResultException;

import static domain.service.JobServiceImpl.normalizeQuery;

@ApplicationScoped
public class JobConsumer {
	@Inject
	JobService jobService;

	// @Inject
	// @Channel("saveNewJob")
	// Emitter<SearchComm> newJobEmitter;

	@Inject
	@Channel("articles")
	Emitter<Article> newArticleEmitter;

	@Inject
	@Channel("updateJob")
	Emitter<SearchComm> newUpdateJobEmitter;

	@Incoming("deleteJob")
	public void deleteJob(Long jobId) {
		Job jobInDB = jobService.get(jobId);
		if (Objects.isNull(jobInDB))
			return;
		jobInDB.setCounter(jobInDB.getCounter() - 1);
		if (jobInDB.getCounter() <= 0)
			jobService.delete(jobId);
		else
			jobService.update(jobInDB, false);
	}

	@Incoming("createJob")
	@Blocking
	public void createJob(SearchComm req) {
		if (req.getKeyWord() == null) {
			return;
		}
		String dnfQuery = normalizeQuery(req.getKeyWord());
		Job jobInDB = null;
		try {
			jobInDB = jobService.get(dnfQuery);
		} catch (IllegalArgumentException | NoResultException ignored) {

		}
		Job job = new Job();
		job.setAccessDate(LocalDate.now());
		if (Objects.isNull(jobInDB)) {
			job.setDnfQuery(dnfQuery);
			job.setJobStatus("IN_PROGRESS");
			job.setCreateDate(LocalDate.now());
			job.setCounter(1L);
			jobService.create(job);
		} else {
			job.setJobId(jobInDB.getJobId());
			job.setCounter(jobInDB.getCounter() + 1);
			jobService.update(job, true);
		}
		// SearchComm send = new SearchComm();
		// send.setSearchId(req.getSearchId());
		// send.setJobId(job.getJobId());
		// send.setJobStatus(job.getJobStatus());
		// newJobEmitter.send(send);
		// Change to emitter.send to send to a search pod when available instead of
		// function call
		// It will also send an updated status
		String csvPath = jobService.launch(job);

		job.setJobStatus("FINISHED");
		jobService.update(job, false);

		SearchComm out = new SearchComm();
		out.setJobId(job.getJobId());
		out.setKeyWord(csvPath);
		out.setJobStatus(job.getJobStatus());

		sendArticles(out);

		out.setKeyWord(null);
		out.setSearchId(req.getSearchId());

		newUpdateJobEmitter.send(out);
	}

	public void sendArticles(SearchComm rec) {
		Long jobId = rec.getJobId();
		for (int i = 1; i < 10; ++i)
			newArticleEmitter.send(
					new Article(jobId, "DOI" + i, "title" + i, "abstract" + i, "url" + i));
	}
}
