drop table Job if exists;
drop sequence if exists JOB_SEQ;
create sequence JOB_SEQ start with 1 increment by 1;


create table Job (
    jobId Bigint(255) not null,
    dnfQuery varchar(255) not null,
    jobStatus varchar(255) not null,
    createDate varchar(255) not null,
    accessDate varchar(255) not null,
    counter Bigint(255) not null,
    primary key (jobId)
);