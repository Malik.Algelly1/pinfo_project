/* (C)2022 */
package api.msg;

import io.quarkus.test.common.QuarkusTestResourceLifecycleManager;
import io.smallrye.reactive.messaging.providers.connectors.InMemoryConnector;

import java.util.HashMap;
import java.util.Map;

public class KafkaTestInResourceLifecycleManager implements QuarkusTestResourceLifecycleManager {

	@Override
	public Map<String, String> start() {
		Map<String, String> env = new HashMap<>();
		Map<String, String> props;
		props = InMemoryConnector.switchIncomingChannelsToInMemory("createJob");
		env.putAll(props);
		props = InMemoryConnector.switchIncomingChannelsToInMemory("deleteJob");
		env.putAll(props);
		return env;
	}

	@Override
	public void stop() {
		InMemoryConnector.clear();
	}
}
