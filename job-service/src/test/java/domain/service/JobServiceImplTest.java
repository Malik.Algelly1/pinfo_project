/* (C)2022 */
package domain.service;

import domain.model.Job;
import io.quarkus.test.junit.QuarkusTest;
import lombok.extern.java.Log;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import java.time.LocalDate;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

import static domain.service.JobServiceImpl.normalizeQuery;

@QuarkusTest
@Log
class JobServiceImplTest {

	@Inject
	EntityManager em;

	@Inject
	JobService jobService;

	@Test
	void testLaunch() {
		Job testJob = getRandomJob();
		String csvPath = System.getProperty("user.dir") + "/src/test/resources/sample.csv";
		;
		assertEquals(csvPath, jobService.launch(testJob));
	}

	@Test
	void testNormalizeQuery() {
		String query = "africa is africa and asia and not europe";
		String queryExpected = "africa is africa & asia & ~ europe";
		log.info("dnfQuery: " + normalizeQuery(query));
		assertEquals(queryExpected, normalizeQuery(query));
	}

	@Test
	void testTransaction() {
		Job testJob = getRandomJob();
		testJob.setJobStatus("IN_PROGRESS");
		Long n = jobService.count();
		Long ns = jobService.count("IN_PROGRESS");

		log.info("Job id in testCreate before: " + testJob.getJobId());

		jobService.create(testJob);
		assertEquals(ns + 1L, jobService.count("IN_PROGRESS"));

		log.info("Job id in testCreate after: " + testJob.getJobId());

		testExists(testJob);

		testGet(testJob);

		Job jobInDB = jobService.get(testJob.getDnfQuery());

		assertEquals(testJob.getJobId(), jobInDB.getJobId());
		assertEquals(testJob.getDnfQuery(), jobInDB.getDnfQuery());

		assertEquals(n + 1L, jobService.count());

		jobService.delete(jobInDB.getJobId());

		assertEquals(n, jobService.count());
	}

	void testGet(Job testJob) {
		Job jobInDBWithId = jobService.get(testJob.getJobId());
		Job jobInDBWithDnfQuery = jobService.get(testJob.getDnfQuery());
		assertEquals(testJob, jobInDBWithId);
		assertEquals(testJob, jobInDBWithDnfQuery);
		assertThrows(NoResultException.class, () -> jobService.get(49L));
		assertThrows(NoResultException.class, () -> jobService.get("Family"));
	}

	void testExists(Job testJob) {
		assertTrue(jobService.exists(testJob.getJobId()));
		assertTrue(jobService.exists(testJob.getDnfQuery()));
		assertFalse(jobService.exists(49L));
		assertFalse(jobService.exists("Family"));
	}

	@Test
	void testUpdate() {
		Job testJob = getRandomJob();
		jobService.create(testJob);

		Long jobId = testJob.getJobId();
		Long counter = testJob.getCounter();
		jobService.update(testJob, true);

		Job jobInDB = jobService.get(jobId);
		assertEquals(counter + 1L, jobInDB.getCounter());
		jobService.delete(jobInDB.getJobId());
	}

	private Job getRandomJob() {
		String[] connector = { " & ", " | " };
		String[] statuses = { "FINISHED", "IN_PROGRESS", "CANCELLED", "ERROR" };
		java.util.Random random = new Random();
		Job job = new Job();
		job.setDnfQuery(RandomStringUtils.randomAlphanumeric(4, 8) + connector[random.nextInt(connector.length)]
				+ RandomStringUtils.randomAlphanumeric(4, 8));
		job.setJobStatus(statuses[random.nextInt(statuses.length)]);
		job.setCreateDate(LocalDate.now());
		job.setAccessDate(LocalDate.now());
		job.setCounter(random.nextLong(1, 5));
		return job;
	}
}
