drop table Job if exists;
drop sequence if exists JOB_SEQ;
create sequence JOB_SEQ start with 1 increment by 1;

create table Job (
    jobId Bigint(255) not null,
    dnfQuery varchar(255) not null,
    jobStatus varchar(255) not null,
    createDate varchar(255) not null,
    accessDate varchar(255) not null,
    counter Bigint(255) not null,
    primary key (jobId)
);

INSERT INTO JOB (jobId, dnfQuery, jobStatus, createDate, accessDate, counter) VALUES (JOB_SEQ.nextval, 'Africa & Asia', 'IN_PROGRESS', '2022-05-10', '2022-05-10', 1);
INSERT INTO JOB (jobId, dnfQuery, jobStatus, createDate, accessDate, counter) VALUES (JOB_SEQ.nextval, 'Africa & Antarctica', 'IN_PROGRESS', '2022-05-10', '2022-05-10', 2);
INSERT INTO JOB (jobId, dnfQuery, jobStatus, createDate, accessDate, counter) VALUES (JOB_SEQ.nextval, 'Africa & Europe', 'FINISHED', '2022-05-10', '2022-05-10', 3);
INSERT INTO JOB (jobId, dnfQuery, jobStatus, createDate, accessDate, counter) VALUES (JOB_SEQ.nextval, 'Africa & Australia', 'ERROR', '2022-05-10', '2022-05-10', 3);
INSERT INTO JOB (jobId, dnfQuery, jobStatus, createDate, accessDate, counter) VALUES (JOB_SEQ.nextval, 'North America & Latin America', 'CANCELLED', '2022-05-10', '2022-05-10', 1);
