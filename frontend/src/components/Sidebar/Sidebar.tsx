import React, { ReactElement } from "react"
import { User } from "auth0"

import { useAuth0 } from "@auth0/auth0-react"
import { NavLink } from "react-router-dom"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import {
    faCog,
    faHistory,
    faSearch,
    faSignOut,
} from "@fortawesome/free-solid-svg-icons"

import "./Sidebar.css"

type Props = {
    user: User
}

const Sidebar = ({ user }: Props) => {
    const { logout } = useAuth0()

    return (
        <div id="sidebar">
            <div className="user-container">
                <img src={user.picture ? user.picture : "placeholder.jpg"} />
                {user.name ? <p>{user.name}</p> : null}
            </div>
            <ul>
                <li>
                    <NavLink
                        to="/"
                        className={({ isActive }) => (isActive ? "active" : "")}
                    >
                        <FontAwesomeIcon className="icon" icon={faSearch} />
                        Search
                    </NavLink>
                </li>
                <li>
                    <NavLink
                        to="history"
                        className={({ isActive }) => (isActive ? "active" : "")}
                    >
                        <FontAwesomeIcon className="icon" icon={faHistory} />
                        History
                    </NavLink>
                </li>
                <li>
                    <NavLink
                        to="settings"
                        className={({ isActive }) => (isActive ? "active" : "")}
                    >
                        <FontAwesomeIcon className="icon" icon={faCog} />
                        Settings
                    </NavLink>
                </li>
                <li>
                    <button
                        onClick={() =>
                            logout({ returnTo: window.location.origin })
                        }
                    >
                        <FontAwesomeIcon className="icon" icon={faSignOut} />
                        Log out
                    </button>
                </li>
            </ul>
        </div>
    )
}

export default Sidebar
