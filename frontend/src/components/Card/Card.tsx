import React from "react"
import "./Card.css"

type Props = {
    children: JSX.Element
    className?: string
}

const Card = ({ children, className }: Props) => {
    return (
        <div className={"card " + (className ? className : "")}>{children}</div>
    )
}

export default Card
