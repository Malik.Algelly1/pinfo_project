import React from "react"
import { JsxElement } from "typescript"

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"

import styles from "./IconButton.module.css"
import { IconDefinition } from "@fortawesome/fontawesome-svg-core"

type Props = {
    children?: any
    icon: IconDefinition
    onClick?: React.MouseEventHandler<HTMLAnchorElement>
    href?: string
    target?: React.HTMLAttributeAnchorTarget
}

const IconButton = ({ children, icon, onClick, href, target }: Props) => {
    return (
        <a
            className={children ? styles.longBtn : styles.btn}
            onClick={onClick}
            href={href}
            target={target}
        >
            <>
                {children}
                <FontAwesomeIcon
                    icon={icon}
                    className={children ? styles.longIcon : styles.icon}
                />
            </>
        </a>
    )
}

export default IconButton
