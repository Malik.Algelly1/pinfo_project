import React from "react"
import ReactDOM from "react-dom/client"
import "./index.css"
import App from "./App"
import reportWebVitals from "./reportWebVitals"
import { Auth0Provider } from "@auth0/auth0-react"
import { BrowserRouter } from "react-router-dom"
import { HelmetProvider, Helmet } from "react-helmet-async"
import Particles from "react-tsparticles"
import { loadFull } from "tsparticles"

const root = ReactDOM.createRoot(document.getElementById("root") as HTMLElement)
root.render(
    <React.StrictMode>
        <BrowserRouter>
            <Auth0Provider
                domain="pinfo4-dev.eu.auth0.com"
                clientId="NNi0EWqD2Tfd8dwN95xIuNI8gfI4z02j"
                redirectUri={window.location.origin}
                audience="https://pinfo4-dev.eu.auth0.com/api/v2/"
                scope="read:profile"
            >
                <HelmetProvider>
                    <Helmet>
                        <link
                            rel="preconnect"
                            href="https://fonts.googleapis.com"
                        />
                        <link
                            rel="preconnect"
                            href="https://fonts.gstatic.com"
                        />
                        <link
                            href="https://fonts.googleapis.com/css2?family=Fira+Sans:wght@400;500;600;700;900&display=swap"
                            rel="stylesheet"
                        />
                    </Helmet>
                    <App />
                </HelmetProvider>
            </Auth0Provider>
        </BrowserRouter>
    </React.StrictMode>
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
