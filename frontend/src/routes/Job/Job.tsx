import React from "react"

import JobRow from "./JobRow"

import styles from "./Job.module.css"

import mock from "./mock.json"

const Job = () => {
    var rows = []
    let i = 0
    for (const article of mock) {
        rows.push(<JobRow key={i} article={article} />)
        i++
    }
    return (
        <table className={styles.job}>
            <tbody>
                <tr>
                    <th className={styles.titleHead}>Title</th>
                    <th className={styles.abstractHead}>Abstract</th>
                    <th className={styles.linkHead}>Link</th>
                </tr>
                {rows}
            </tbody>
        </table>
    )
}

export default Job
