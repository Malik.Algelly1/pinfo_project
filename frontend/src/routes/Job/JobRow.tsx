import React from "react"

import IconButton from "../../components/IconButton/IconButton"
import { faLink } from "@fortawesome/free-solid-svg-icons"

import styles from "./Job.module.css"

type Props = {
    article: {
        articleId: Number
        doi: string
        title: string
        articleAbstract: string
        url: string
    }
}

const JobRow = ({ article }: Props) => {
    return (
        <tr>
            <td className={styles.titleCell}>{article.title}</td>
            <td className={styles.abstractCell}>{article.articleAbstract}</td>
            <td className={styles.linkCell}>
                <IconButton icon={faLink} href={article.url} target="_blank" />
            </td>
        </tr>
    )
}

export default JobRow
