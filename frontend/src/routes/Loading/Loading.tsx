import React from "react"
import { ThreeCircles } from "react-loader-spinner"

import styles from "./Loading.module.css"

const Loading = () => {
    return (
        <div className={styles.loader}>
            <ThreeCircles
                color="white"
                height={50}
                width={50}
                ariaLabel="three-circles-rotating"
            />
        </div>
    )
}

export default Loading
