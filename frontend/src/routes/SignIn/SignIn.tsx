import React from "react"

import { useAuth0 } from "@auth0/auth0-react"
import { faArrowRightToBracket } from "@fortawesome/free-solid-svg-icons"
import IconButton from "../../components/IconButton/IconButton"
import Particles from "react-tsparticles"
import { loadFull } from "tsparticles"
import Card from "../../components/Card/Card"

import { particlesConfig } from "./particles-config"

import styles from "./SignIn.module.css"

const particlesInit = async (main: any) => {
    await loadFull(main)
}

const SignIn = () => {
    const { loginWithRedirect } = useAuth0()

    return (
        <>
            <div id="particles-container">
                <Particles
                    id="particles"
                    init={particlesInit}
                    options={particlesConfig}
                />
            </div>
            <Card className="centered">
                <>
                    <img src="logo.png" alt="SRAS" className={styles.logo} />
                    <p className={styles.msg}>
                        This service is only accessible if you've created an
                        account.
                    </p>

                    <IconButton
                        icon={faArrowRightToBracket}
                        onClick={loginWithRedirect}
                    >
                        Log in <span>or</span> Sign up
                    </IconButton>
                </>
            </Card>
        </>
    )
}

export default SignIn
