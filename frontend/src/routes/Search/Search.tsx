import React, { useEffect, useState } from "react"

import { useAuth0 } from "@auth0/auth0-react"
import { faRocket } from "@fortawesome/free-solid-svg-icons"
import IconButton from "../../components/IconButton/IconButton"

import "./Search.css"

const Search = () => {
    const { user, getAccessTokenSilently } = useAuth0()
    const [token, setToken] = useState("")
    const [query, setQuery] = useState("")

    const postSearch = async (query: string, email?: string) => {
        console.log("query", query)
        await fetch("https://pinfo4.unige.ch/api/searches", {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${token}`,
                // "Access-Control-Allow-Origin": "https://pinfo4.unige.ch",
            },
            // mode: "no-cors",
            body: JSON.stringify({ keyWord: query, email: email }),
        })
            // .then((response) => response.json())
            .then((response) => {
                console.log(response.status)
                console.log(`Okay ? ${response.ok}`)
            })
            .catch((err) => console.error(err))
    }

    useEffect(() => {
        ;(async () => {
            try {
                const token = await getAccessTokenSilently({
                    audience: "https://pinfo4-dev.eu.auth0.com/api/v2/",
                    scope: "read:profile",
                })
                setToken(token)
                console.log("token", token)
            } catch (e) {
                console.error(e)
            }
        })()
    }, [getAccessTokenSilently])

    return (
        <>
            <textarea
                name="search"
                id="search"
                rows={7}
                cols={40}
                onChange={(e) => setQuery(e.target.value)}
            ></textarea>
            <IconButton
                icon={faRocket}
                onClick={() => {
                    postSearch(
                        query,
                        user
                            ? user.email_verified
                                ? user.email
                                : undefined
                            : undefined
                    )
                }}
            >
                Launch Search
            </IconButton>
        </>
    )
}

export default Search
