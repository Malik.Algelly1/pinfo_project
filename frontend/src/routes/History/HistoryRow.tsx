import React from "react"

import Status from "./Status"
import IconButton from "../../components/IconButton/IconButton"
import { faEllipsis } from "@fortawesome/free-solid-svg-icons"
import { Link } from "react-router-dom"

import styles from "./History.module.css"

type Props = {
    search: {
        query: string
        date: string
        status: string
    }
}

const HistoryRow = ({ search }: Props) => {
    return (
        <tr>
            <td className={styles.queryCell}>
                <Link to={"1"}>{search.query}</Link>
            </td>
            <td className={styles.dateCell}>
                <Link to={"1"}>{search.date}</Link>
            </td>
            <td className={styles.statusCell}>
                <Status status={search.status} />
            </td>
            <td className={styles.actionCell}>
                <IconButton icon={faEllipsis} />
            </td>
        </tr>
    )
}

export default HistoryRow
