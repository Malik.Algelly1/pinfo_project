import React from "react"

import "./Status.css"

type Props = {
    status: string
}

const statuses = [
    "running",
    "stopped",
    "failed",
    "finished",
    "expired",
    "aborted",
]

const Status = ({ status }: Props) => {
    if (statuses.includes(status))
        return <div className={"status " + status}>{status}</div>
    return null
}

export default Status
