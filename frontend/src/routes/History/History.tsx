import React from "react"

import HistoryRow from "./HistoryRow"

import styles from "./History.module.css"

import mock from "./mock.json"

const History = () => {
    var rows = []
    let i = 0
    for (const search of mock) {
        rows.push(<HistoryRow key={i} search={search} />)
        i++
    }
    return (
        <table className={styles.history}>
            <tbody>
                <tr>
                    <th>Query</th>
                    <th>Date</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
                {rows}
            </tbody>
        </table>
    )
}

export default History
