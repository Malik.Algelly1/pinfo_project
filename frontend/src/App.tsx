import React from "react"
import logo from "./logo.svg"
import "./App.css"

import { useAuth0 } from "@auth0/auth0-react"
import { Routes, Route } from "react-router-dom"

import Sidebar from "./components/Sidebar/Sidebar"
import Card from "./components/Card/Card"

import Loading from "./routes/Loading/Loading"
import Search from "./routes/Search/Search"
import History from "./routes/History/History"
import Settings from "./routes/Settings/Settings"
import Job from "./routes/Job/Job"
import SignIn from "./routes/SignIn/SignIn"

const App = () => {
    const { user, isAuthenticated, isLoading } = useAuth0()

    if (isLoading) {
        return <Loading />
    }

    if (isAuthenticated && user) {
        return (
            <div id="app">
                <Sidebar user={user} />
                <div id="page">
                    <Card>
                        <Routes>
                            <Route index element={<Search />} />
                            <Route path="history">
                                <Route index element={<History />} />
                                <Route path=":id" element={<Job />} />
                            </Route>
                            <Route path="settings" element={<Settings />} />
                            <Route path="job" element={<Job />} />
                            <Route path="*" element={<Search />} />
                        </Routes>
                    </Card>
                </div>
            </div>
        )
    }

    return <SignIn />
}

export default App
